package info.learncoding.starred_rep.utils

object AppConstraint {
    const val DATABASE_NAME = "git_repo"
    const val DATABASE_VERSION = 1

    const val GIT_REPO_SEARCH_KEY = "Android"
    const val GIT_REPO_SORT = "stars"
}