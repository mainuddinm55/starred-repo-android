package info.learncoding.starred_rep.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import info.learncoding.starred_rep.data.local.daos.GitRepoDao
import info.learncoding.starred_rep.data.local.daos.PaginationKeyDao
import info.learncoding.starred_rep.data.models.GitRepo
import info.learncoding.starred_rep.data.models.PaginationKey
import info.learncoding.starred_rep.utils.AppConstraint.DATABASE_VERSION

@Database(
    entities = [GitRepo::class, PaginationKey::class],
    version = DATABASE_VERSION,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun gitRepoDao(): GitRepoDao

    abstract fun paginationKeyDao(): PaginationKeyDao

    companion object {
        val migrations = arrayOf<Migration>()
    }
}