package info.learncoding.starred_rep.data.local.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class StringArrayConverter {

    @TypeConverter
    fun toLicense(json: String?): List<String> {
        return Gson().fromJson(json, object : TypeToken<List<String>>() {}.type)
    }

    @TypeConverter
    fun fromLicense(list: List<String>?): String? {
        return Gson().toJson(list)
    }
}