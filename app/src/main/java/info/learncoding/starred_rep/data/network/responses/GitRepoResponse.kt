package info.learncoding.starred_rep.data.network.responses

import com.google.gson.annotations.SerializedName
import info.learncoding.starred_rep.data.models.GitRepo

data class GitRepoResponse(
    @SerializedName("total_count") var totalCount: Int? = null,
    @SerializedName("incomplete_results") var incompleteResults: Boolean? = null,
    @SerializedName("items") var items: ArrayList<GitRepo> = arrayListOf()
)