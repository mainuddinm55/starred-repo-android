package info.learncoding.starred_rep.data.local.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import info.learncoding.starred_rep.data.models.Owner

class OwnerConverter {

    @TypeConverter
    fun toOwner(json: String?): Owner? {
        return Gson().fromJson(json, Owner::class.java)
    }

    @TypeConverter
    fun fromOwner(owner: Owner?): String? {
        return Gson().toJson(owner)
    }
}