package info.learncoding.starred_rep.data.local.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import info.learncoding.starred_rep.data.models.License

class LicenseConverter {

    @TypeConverter
    fun toLicense(json: String?): License? {
        return Gson().fromJson(json, License::class.java)
    }

    @TypeConverter
    fun fromLicense(license: License?): String? {
        return Gson().toJson(license)
    }
}