package info.learncoding.starred_rep.data.network

import info.learncoding.starred_rep.data.models.Error

sealed class ApiResponse<out T> {
    data class Success<out T>(val data: T) : ApiResponse<T>()
    data class Failed(val error: Error) : ApiResponse<Nothing>()
}