package info.learncoding.starred_rep.data.models

sealed class Error(val msg: String) {
    data class NotFound(private val message: String) : Error(message)
    object ValidationError : Error("Missing required field")
    object InternalServerError : Error("Internal server error")
    object Unauthorized : Error("Unauthorized")
    object Forbidden : Error("Forbidden")
    object Network : Error("No internet connection")
    data class Failure(private val message: String) : Error(message)
}
