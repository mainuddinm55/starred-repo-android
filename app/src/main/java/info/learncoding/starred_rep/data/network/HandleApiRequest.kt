package info.learncoding.starred_rep.data.network

import info.learncoding.starred_rep.data.models.Error
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

abstract class HandleApiRequest {

    suspend fun <T> apiRequest(apiCall: suspend () -> T): ApiResponse<T> {
        return withContext(Dispatchers.IO) {
            try {
                ApiResponse.Success(apiCall.invoke())
            } catch (e: Exception) {
                val error: Error
                e.printStackTrace()
                when (e) {
                    is IOException -> {
                        error = Error.Network
                    }
                    is HttpException -> {
                        error = when (e.code()) {
                            400 -> Error.Failure("Invalid request, please try again")
                            204 -> Error.NotFound("No Data Found")
                            401 -> Error.Unauthorized
                            403 -> Error.Forbidden
                            404 -> Error.NotFound("Request not found")
                            422 -> Error.ValidationError
                            500 -> Error.InternalServerError
                            else -> Error.Failure(e.message())
                        }
                    }
                    else -> {
                        error = Error.Failure("Something went wrong, please try again")
                    }
                }
                ApiResponse.Failed(error)
            }
        }
    }

}