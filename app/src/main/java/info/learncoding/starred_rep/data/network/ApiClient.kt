package info.learncoding.starred_rep.data.network

import info.learncoding.starred_rep.data.network.responses.GitRepoResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiClient {
    @GET("search/repositories")
    suspend fun searchRepositories(
        @Query("q") query: String,
        @Query("page") page: Int,
        @Query("per_page") pageSize: Int,
        @Query("sort") sort: String
    ): GitRepoResponse
}