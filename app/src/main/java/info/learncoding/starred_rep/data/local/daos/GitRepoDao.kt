package info.learncoding.starred_rep.data.local.daos

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import info.learncoding.starred_rep.data.models.GitRepo

@Dao
interface GitRepoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(repos: List<GitRepo>)

    @Query("SELECT * FROM GitRepo ORDER BY :order_by DESC")
    fun getGitRepositories(order_by: String): PagingSource<Int, GitRepo>

    @Query("DELETE FROM GitRepo")
    suspend fun deleteAll()
}