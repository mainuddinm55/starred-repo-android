package info.learncoding.starred_rep.data.local.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import info.learncoding.starred_rep.data.models.PaginationKey

@Dao
interface PaginationKeyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(keys: List<PaginationKey>)

    @Query("SELECT * FROM PaginationKey WHERE id = :id")
    suspend fun get(id: Int): PaginationKey?

    @Query("DELETE FROM PaginationKey")
    suspend fun deleteAll()

}