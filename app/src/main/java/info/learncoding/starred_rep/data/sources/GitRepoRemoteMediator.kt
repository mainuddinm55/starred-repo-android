package info.learncoding.starred_rep.data.sources

import android.util.Log
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import info.learncoding.starred_rep.data.local.AppDatabase
import info.learncoding.starred_rep.data.models.GitRepo
import info.learncoding.starred_rep.data.models.PaginationKey
import info.learncoding.starred_rep.data.network.ApiClient
import info.learncoding.starred_rep.utils.AppConstraint.GIT_REPO_SEARCH_KEY
import info.learncoding.starred_rep.utils.AppConstraint.GIT_REPO_SORT

@OptIn(ExperimentalPagingApi::class)
class GitRepoRemoteMediator(
    private val database: AppDatabase,
    private val apiClient: ApiClient
) : RemoteMediator<Int, GitRepo>() {

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, GitRepo>
    ): MediatorResult {
        try {
            val page = when (loadType) {
                LoadType.REFRESH -> 1
                LoadType.PREPEND -> return MediatorResult.Success(false)
                LoadType.APPEND -> {
                    val paginationKey = getKeyForLastItem(state)
                    Log.d("Pagination", "load: nextKey: $paginationKey")
                    if (paginationKey?.nextKey == null) {
                        return MediatorResult.Success(true)
                    }
                    paginationKey.nextKey
                }
            }
            val response = apiClient.searchRepositories(
                GIT_REPO_SEARCH_KEY,
                page,
                state.config.pageSize,
                GIT_REPO_SORT
            )
            database.withTransaction {
                if (loadType == LoadType.REFRESH) {
                    database.paginationKeyDao().deleteAll()
                    database.gitRepoDao().deleteAll()
                }
            }
            val nextKey = if (response.items.isEmpty()) null else page.plus(1)
            val paginationKeys = response.items.map { PaginationKey(it.id, nextKey) }
            database.withTransaction {
                database.paginationKeyDao().insertAll(paginationKeys)
                database.gitRepoDao().insert(response.items)
            }
            return MediatorResult.Success(nextKey == null)
        } catch (e: Exception) {
            return MediatorResult.Error(e)
        }
    }

    private suspend fun getKeyForLastItem(state: PagingState<Int, GitRepo>): PaginationKey? {
        return state.lastItemOrNull()?.let {
            Log.d("Pagination", "getKeyForLastItem: nextKey: $it")
            database.paginationKeyDao().get(it.id)
        }
    }
}