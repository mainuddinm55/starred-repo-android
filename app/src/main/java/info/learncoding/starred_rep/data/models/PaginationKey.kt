package info.learncoding.starred_rep.data.models


import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PaginationKey(
    @PrimaryKey
    val id: Int,
    val nextKey: Int?
)